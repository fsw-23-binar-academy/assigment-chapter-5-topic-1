const config = require("../config");
const fs = require("fs");
class Model {
  constructor() {
    this.db = config.read_database;
  }

  getAllData() {
    const data = this.db;
    return this.formattingData(data);
  }

  getById(id) {
    const data = this.getAllData();
    let out = false;
    data.forEach((item) => {
      if (item.id == id) {
        out = item;
        return;
      }
    });
    return out;
  }

  addData(data) {
    const students = this.getAllData();
    students.push(data);
    const replacing = config.replace_database(students);
    console.log(replacing);
    if (!replacing) {
      return false;
    }
    return true;
  }

  editData(id, newData) {
    const students = this.getAllData();
    students.forEach((student, index) => {
      if (student.id == id) {
        students[index] = { ...student, ...newData };
      }
    });
    const replacing = config.replace_database(students);
    if (!replacing) {
      return false;
    }
    return true;
  }

  deleteData(id) {
    const students = this.getAllData();
    const studentsFiltered = students.filter((student) => {
      return student.id != id;
    });

    const replacing = config.replace_database(studentsFiltered);
    if (!replacing) {
      return false;
    }
    return true;
  }

  formattingData(data) {
    return JSON.parse(data);
  }
}

module.exports = Model;
