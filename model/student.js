const Model = require("./model");

class Student extends Model {
  constructor() {
    super();
  }

  getAllStudent() {
    return super.getAllData();
  }

  getDetailStudent(id) {
    return super.getById(id);
  }

  addStudent(params) {
    return super.addData(params);
  }

  editStudent(id, params) {
    return super.editData(id, params);
  }

  deleteStudent(id) {
    return super.deleteData(id);
  }
}

module.exports = Student;
