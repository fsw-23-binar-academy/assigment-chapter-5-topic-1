const APP = require("./controller/app");
const MODEL = require("./model/student");

const modelStudents = new MODEL();
const managementStudentsApp = new APP({
  model: modelStudents,
});

if (process.argv[2] == "getAllStudent") {
  const request = managementStudentsApp.getAllStudent();
  console.log(request);
}

if (process.argv[2] == "getDetailStudent") {
  const idStudent = process.argv[3];
  const request = managementStudentsApp.getDetailStudent(idStudent);
  console.log(request);
}

if (process.argv[2] == "addStudent") {
  // how to run is : node index.js addStudent '{"name":"Daniel", "gender":"male"}'
  const params = process.argv[3];
  const student = JSON.parse(params);
  console.log(student.hobby.length);
  // const request = managementStudentsApp.addStudent(student);
  // console.log(request);
}

if (process.argv[2] == "editStudent") {
  // how to run is : node index.js editStudent 2 '{"name":"Daniel Winata", "gender":"female"}'
  const idStudent = process.argv[3];
  const params = process.argv[4];
  const student = JSON.parse(params);
  const request = managementStudentsApp.editStudent(idStudent, student);
  console.log(request);
}

if (process.argv[2] == "deleteStudent") {
  // how to run is : node index.js deleteStudent 2
  const idStudent = process.argv[3];
  const request = managementStudentsApp.deleteStudent(idStudent);
  console.log(request);
}
