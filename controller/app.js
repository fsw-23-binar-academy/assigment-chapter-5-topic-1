class Apps {
  constructor(props) {
    this.model = props.model;
  }

  getAllStudent() {
    const data = this.model.getAllStudent();
    return {
      data,
    };
  }

  getDetailStudent(id) {
    const student = this.model.getDetailStudent(id);
    if (student) {
      return this.response(`Success get data student for id ${id}`, student);
    }

    return this.response("Student has not exist", {});
  }

  addStudent(params) {
    const id = this.generateNewStudentId();
    const { name, gender } = params;
    if (name.match(/\d+/g)) {
      return this.response(
        "Name of students cannot contain numbers",
        params,
        false
      );
    }

    const request = this.model.addStudent({
      id,
      name,
      gender,
    });

    if (request) {
      return this.response(`Success add data student ${name}`, {
        id,
        name,
        gender,
      });
    }
    return this.response("Error when adding user to database", {
      id,
      name,
      gender,
    });
  }

  editStudent(id, newData) {
    const request = this.model.editStudent(id, newData);
    if (request) {
      return this.response(`Success edit data student ${id}`, newData);
    }
    return this.response("Error when adding user to database", newData);
  }

  deleteStudent(id) {
    const request = this.model.deleteStudent(id);
    if (request) {
      return this.response(`Success delete data student ${id}`);
    }
    return this.response(`Error when delete user in database ${id}`);
  }

  generateNewStudentId() {
    const data = this.model.getAllStudent();
    const id = data[data.length - 1].id + 1;
    return id;
  }

  response(message, data, success = true) {
    return {
      message,
      data,
      success: success,
    };
  }
}

module.exports = Apps;
