const fs = require("fs");
const PATH_DB = "./db/students.json";
const database = fs.readFileSync(PATH_DB, "utf-8");
const config = {
  read_database: database,
  replace_database: (data) => {
    fs.writeFileSync(PATH_DB, JSON.stringify(data));
    return true;
  },
};

module.exports = config;
